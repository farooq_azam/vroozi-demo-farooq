import angular from 'angular';
import '../style/app.css';


let app = () => {
  return {
    template: require('./app.html'),
    controller: 'AppCtrl',
    controllerAs: 'todos'
  }
};


class AppCtrl {
  constructor() {
      this.text;
      this.todoStorage = [];
      this.edit = false;
      this.newTodo;
      this.isComplete;
      this.addTodo = this.addTodo.bind(this);
      this.deleteTodo = this.deleteTodo.bind(this);
      this.editTodo = this.editTodo.bind(this);
  }
  addTodo(todo){
      this.todoStorage.push(this.text);
      this.text = '';
      
  }
  deleteTodo(todo){
      let index = this.todoStorage.indexOf(todo);
      this.todoStorage.splice(index , 1);
    }
    
  editTodo(todo){
      this.edit = !this.edit;
      let index = this.todoStorage.indexOf(todo);
      console.log(this.newTodo);
      
  }
  
}

const MODULE_NAME = 'app';

angular.module(MODULE_NAME, [])
  .directive('app', app)
  .controller('AppCtrl', AppCtrl);

export default MODULE_NAME;